<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
		<link rel="stylesheet" type="text/css" href="index.css">
    </head>
    <body>
		<div id="container">
			<span class="title">Login/Sign Up</span>
			<!--creates form that makes login terminal-->
			<form method="POST">
				<input type="text" name="username" /> <br>
				<input type="submit" name="submit" value="Login" />
				<input type="submit" name="signup" value="Sign Up"/> <br>
			</form>
		</div>
		<?php
			//checks if user name is legit
			if(isset($_POST['submit'])) {
				session_start();
				$_SESSION['username'] = $_POST['username'];
				$is_legit = false;
				$users_file = fopen("/srv/users.txt", 'r');
				while(!feof($users_file)) {
					$user = trim(fgets($users_file));
					if($_SESSION['username'] == $user) {
						$is_legit = true;
						break;
					}
				}
				fclose($users_file);
				if($is_legit) {
					header("Location: fileUpload.php");
					exit;
				}
				else {
					echo "<div class='invalid'>Invalid username.</div>";
				}
			}
			//checks if user name has already been used when signing up
			if(isset($_POST['signup'])){
				$is_legit = true;
				$users_file = fopen("/srv/users.txt", 'r+');
				while(!feof($users_file)) {
					$user = trim(fgets($users_file));
					if($_POST['username'] == '')
					{
						$is_legit=false;
						break;
					}
					if($_POST['username'] == $user) {
						$is_legit =false;
						break;
					}
				}
				if($is_legit)
				{
					fwrite($users_file, "\n".$_POST['username']);
					echo "<div style='text-align:center'>Signed Up. Please login</div>";
					mkdir("/srv/users/".$_POST['username'], 0777, true);
				}
				else{
					echo "<div class='invalid'>Username already used. Please try again</div>";
				}
				fclose($users_file);
				
			}
		?>
    </body>
</html>