<?php
     session_start();
    //checks if username is valid and file can be transfered
    if(isset($_POST["file_name"]) AND isset($_POST["attemptedUser"]))
    {
        $_SESSION['fileTransferStatus'] = '';
        $filename = $_POST["file_name"];
        $attemptedUser = $_POST["attemptedUser"];
        $path = sprintf("/srv/users/%s/%s", $attemptedUser, $filename);
        $filePath = sprintf("/srv/users/%s/%s", $_SESSION["username"], $filename);
        $is_legit = false;
        $users_file = fopen("/srv/users.txt", 'r');
        while(!feof($users_file)) {
            $user = trim(fgets($users_file));
            if($attemptedUser == $user) {
                $is_legit = true;
                break;
            }
        }
        fclose($users_file);
        if($is_legit) {
            copy($filePath, $path);
            header("Location: fileUpload.php");
            exit;
        }
        else {
            header("Location: transfer.php");
            $_SESSION['fileTransferStatus'] = 'Invalid User.';
            exit;
        }
    }
    else
    {
       $_SESSION['fileTransferStatus']=''; 
    }
?>