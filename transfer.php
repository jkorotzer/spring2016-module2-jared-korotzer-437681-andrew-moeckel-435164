<?php
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Transfer a file!</title>
    <link rel="stylesheet" type="text/css" href="file_transfer.css">
</head>
<body>
    <div id="container">
    Please enter the username you would like to transfer the file to and then click the file you would like to transfer.
    <br>
    <?php
    //shows if file transfered sucsessfully
    if(isset($_SESSION['fileTransferStatus'])) {
        if($_SESSION['fileTransferStatus'] != "") {
            echo $_SESSION['fileTransferStatus'];
        }
    }
    ?>
    <!--allows user to choose which file is being transfered and to who-->
    <form action="file_transfer.php" method="POST">
        <input type="text" name="attemptedUser" value="" id="userbox" />
        <br>
        <?php
            $user = $_SESSION['username'];
            $directory = sprintf("/srv/users/%s", $user);
            $path = "http://ec2-52-89-127-223.us-west-2.compute.amazonaws.com/srv";
            //http://php.net/manual/en/function.readdir.php
            if ($handle = opendir($directory)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        echo
                        '<input type="submit" name="file_name" value="'.$entry.'" />
                        <br>';
                    }
                }
            }
        ?>
    </form>
    </div>
</body>
</html>