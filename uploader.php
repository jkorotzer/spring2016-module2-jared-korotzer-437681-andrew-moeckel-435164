<?php
    session_start();
    
    $filename = basename($_FILES['uploadedfile']['name']);
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ) {
		echo "Invalid filename";
		exit;
	}
    
	$full_path = sprintf("/srv/users/%s/%s", $_SESSION['username'], $filename);
 
	if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path)) {
		header("Location: fileUpload.php");
		exit;
	} else {
        echo "failure";
	}
?>