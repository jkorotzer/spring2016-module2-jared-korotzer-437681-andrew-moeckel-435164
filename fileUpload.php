<?php
	//increases max file upload size and time allowed for file to upload
	ini_set('upload_max_filesize', '10M');
	ini_set('post_max_size', '10M');
	ini_set('max_input_time', 300);
	ini_set('max_execution_time', 300);
	session_start();
	$user = $_SESSION['username'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>File Upload</title>
		<link rel="stylesheet" type="text/css" href="fileUpload.css">
    </head>
    <body>
		<div id="upload">
			<form enctype="multipart/form-data" action="uploader.php" method="POST">
			<p>
				<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
				<label for="uploadfile_input"><?php echo "Hello, ".$user."! " ?>Please choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
			</p>
			<p>
				<input type="submit" value="Upload File" />
			</p>
			</form>
		</div>
		<br>
		<div id="header">
		Or, select any file to view it, or simply check delete and then select the file to delete it!
		</div>
		<?php
		echo "<br>";
		$directory = sprintf("/srv/users/%s", $user);
		$path = "http://ec2-52-89-127-223.us-west-2.compute.amazonaws.com/srv";
		//echoes the name of the files in users directory and makes them buttons
		//http://php.net/manual/en/function.readdir.php
		if ($handle = opendir($directory)) {
			while (false !== ($entry = readdir($handle))) {
				 if ($entry != "." && $entry != "..") {
					echo
					'<form action="file_viewer.php" method="POST">
					<input type="submit" name="file_name" value="'.$entry.'" />
					<input type="checkbox" name="delete" value="true"/>Delete<br>
					</form>';
			 }
		}
		
    	closedir($handle);

		}

		?>
		<br>
		<br>
		Or, click the link below to transfer files to another user!<br>
		<a href="transfer.php">Transfer</a>
		<br>
		<br>
		<form action="logout.php" method="POST">
			<input type ="submit" name="logout_button" value = "Logout"/>
		</form>
	</body>
</html>