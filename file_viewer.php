<?php
    //increases max file upload size and time allowed for file to upload
    ini_set('upload_max_filesize', '10M');
    ini_set('post_max_size', '10M');
    ini_set('max_input_time', 300);
    ini_set('max_execution_time', 300);
    
    session_start();
    $filename = $_POST["file_name"];
    $path = sprintf("/srv/users/%s/%s", $_SESSION['username'], $filename);
    //checks if file is being deleted vs. downloaded
    if(isset($_POST['file_name']) AND isset($_POST['delete'])) {
        unlink($path);
        header("Location: fileUpload.php");
        exit;
    }
    //if file is being downloaded
    else{
        // Get the username and make sure that it is alphanumeric with limited other characters.
        // You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
        // since we will be concatenating the string to load files from the filesystem.
        $username = $_SESSION['username'];
        if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
        }
         
        $full_path = sprintf("/srv/users/%s/%s", $username, $filename);
         
        // Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($full_path);
         
        // Finally, set the Content-Type header to the MIME type of the file, and display the file.
        header("Content-Type: ".$mime);
        readfile($full_path);
    }
    
?>